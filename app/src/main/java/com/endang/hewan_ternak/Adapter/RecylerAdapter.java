package com.endang.hewan_ternak.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import com.endang.hewan_ternak.MainActivity;
import com.endang.hewan_ternak.R;
import com.endang.hewan_ternak.Model.DataModel;

/**
 * Created by jonathansr on 21-Feb-18.
 */

public class RecylerAdapter extends
        RecyclerView.Adapter<RecylerAdapter.MyHolder> {List<DataModel> mList ;
    Context ctx;
    public RecylerAdapter(Context ctx, List<DataModel> mList) {
        this.mList = mList;
        this.ctx = ctx;
    }
    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutlist, parent, false);
        MyHolder holder = new MyHolder(layout);
        return holder;
    }
    @Override
    public void onBindViewHolder(MyHolder holder,
                                 final int position) {
        holder.nama.setText(mList.get(position).getnama());
        holder.kategori.setText(mList.get(position).getkategori());
        holder.jumlah.setText(mList.get(position).getjumlah());
        holder.pemilik.setText(mList.get(position).getpemilik());
        holder.harga.setText(mList.get(position).getharga());
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {@Override
        public void onClick(View view) {
            Intent goInput = new Intent(ctx,MainActivity.class);
            try {
                goInput.putExtra("id", mList.get(position).getId());
                goInput.putExtra("nama", mList.get(position).getnama());
                goInput.putExtra("kategori", mList.get(position).getkategori());
                goInput.putExtra("jumlah", mList.get(position).getjumlah());
                goInput.putExtra("pemilik", mList.get(position).getpemilik());
                goInput.putExtra("harga", mList.get(position).getharga());
                ctx.startActivity(goInput);
            }catch (Exception e){
                e.printStackTrace();
                Toast.makeText(ctx, "Error data " +e, Toast.LENGTH_SHORT).show();
            }
        }
        });
    }
    @Override
    public int getItemCount()
    {
        return mList.size();
    }
    public class MyHolder extends RecyclerView.ViewHolder {
        TextView nama,kategori,jumlah,pemilik,harga;
        DataModel dataModel;
        public MyHolder(View v)
        {
            super(v);
            nama = (TextView) v.findViewById(R.id.tvnama);
            kategori = (TextView) v.findViewById(R.id.tvkategori);
            jumlah = (TextView) v.findViewById(R.id.tvjumlah);
            pemilik = (TextView) v.findViewById(R.id.tvpemilik);
            harga = (TextView) v.findViewById(R.id.tvharga);
        }
    }
}