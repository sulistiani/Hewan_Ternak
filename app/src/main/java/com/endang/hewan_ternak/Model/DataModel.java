package com.endang.hewan_ternak.Model;

import com.google.gson.annotations.SerializedName;

public class DataModel {

    @SerializedName("id")
    private String mId;
    @SerializedName("nama")
    private String mnama;
    @SerializedName("kategori")
    private String mkategori;
    @SerializedName("jumlah")
    private String mjumlah;
    @SerializedName("pemilik")
    private String mpemilik;
    @SerializedName("harga")
    private String mharga;
    

    public String getId() {
        return mId;
    }
    public void setId(String id) {
        mId = id;
    }
    public String getnama() {
        return mnama;
    }
    public void setnama(String nama) {
        mnama = nama;
    }
    public String getkategori() {
        return mkategori;
    }
    public void setkategori(String kategori) {
        mkategori = kategori;
    }
    public String getjumlah() {
        return mjumlah;
    }
    public void setjumlah(String jumlah) {
        mjumlah = jumlah;
    }
    public String getpemilik() {
        return mpemilik;
    }
    public void setpemilik(String pemilik) {
        mpemilik = pemilik;
    }
    public String getharga() {
        return mharga;
    }
    public void setharga(String harga) {
        mharga = harga;
    }
}
