package com.endang.hewan_ternak.Api;

import com.endang.hewan_ternak.Model.ResponseModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RestApi {
    @FormUrlEncoded
    @POST("insert.php")
    Call<ResponseModel> sendBiodata(@Field("nama") String nama,
                                    @Field("kategori") String kategori,
                                    @Field("jumlah") String jumlah,
                                    @Field("pemilik") String pemilik,
                                    @Field("harga") String harga);

                                    @GET("read.php")
                                            Call<ResponseModel> getBiodata();
    //update menggunakan 3 parameter
    @FormUrlEncoded
    @POST("update.php")
    Call<ResponseModel> updateData(@Field("id") String id,
                                   @Field("nama") String nama,
                                   @Field("kategori") String kategori,
                                   @Field("jumlah") String jumlah,
                                   @Field("pemilik") String pemilik,
                                   @Field("harga") String harga);

                                   @FormUrlEncoded
                                   @POST("delete.php")
                                           Call<ResponseModel> deleteData(@Field("id") String id);
}
